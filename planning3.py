#!/usr/bin/env python3

#Pyhon 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import numpy as np
import sys
import copy
import rospy
import moveit_commander #import the moveit_commander namespace
#provides MoveGroupCommander class, PlanningSceneInterface class, and RobotCommander class
import moveit_msgs.msg #import messages used
import geometry_msgs.msg #import messages

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

#Initialize moveit_commander and a rospy node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

#Instantiate a RobotCommander object and a PlanningSceneInterface object
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

#Instantiate MoveGroupCommander, an interface to a planning group (a group of joints)
move_group = moveit_commander.MoveGroupCommander("manipulator")
#Display trajectories in Rviz
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# We get the joint values from the group and change some of the values:
tau = 2*np.pi # 1 revolute
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0 # base
joint_goal[1] = -tau/4 # shoulder
joint_goal[2] = tau/4 # elbow
joint_goal[3] = -tau/2 # wrist1
joint_goal[4] = -tau/4 # wrist2
joint_goal[5] = 0 # wrist3/tool

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

# find and print current joint poses as needed
#joint0 = move_group.get_current_joint_values()
#print("joint0: ",joint0)

"""
# another way to initialize it, can replace the joint_goal with pose_goal
# however, it might get very messy as the arm tries to go all the way around
# joint angles become messy, so decided to swtich to the joint goal method
# First go to initial position (0.5,0.0,0.6) with no rotation
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.x = 0.0
pose_goal.orientation.y = 0.0
pose_goal.orientation.w = 0.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.0
pose_goal.position.z = 0.6
"""

# Start writinng letters
scale = 1.0
waypoints = [] # define waypoints

# Get and print current pose
# move_group.set_pose_target(pose_goal)
wpose = move_group.get_current_pose().pose
print('Initial position: ', wpose)

# 1 WRITE M
# 1.1 go upwards
wpose.position.z += scale * 0.1  # Move up (z)
waypoints.append(copy.deepcopy(wpose))

# 1.2 zigzag down
wpose.position.y += scale * 0.05  
wpose.position.z -= scale * 0.05
waypoints.append(copy.deepcopy(wpose))

# 1.3 zigzag up
wpose.position.y += scale * 0.05
wpose.position.z += scale * 0.05
waypoints.append(copy.deepcopy(wpose))

# 1.4 go downwards
wpose.position.z -= scale * 0.1  # Move down (z)
waypoints.append(copy.deepcopy(wpose))

# 1.5 go back to initial position
wpose.position.y -= scale * 0.1
wpose.position.z += scale * 0.1
waypoints.append(copy.deepcopy(wpose))
waypoints.append(copy.deepcopy(wpose))

# 2 WRITE Z
# 2.1 move horizontally
wpose.position.y += scale * 0.1
waypoints.append(copy.deepcopy(wpose))

# 2.2 zigzag down
wpose.position.y -= scale * 0.1
wpose.position.z -= scale * 0.1
waypoints.append(copy.deepcopy(wpose))

# 2.3 move horizontally again
wpose.position.y += scale * 0.1
waypoints.append(copy.deepcopy(wpose))

# 2.4 go back to initial position
wpose.position.y -= scale * 0.1 
wpose.position.z += scale * 0.1
waypoints.append(copy.deepcopy(wpose))

# 3 WRITE T
# 3.1 move horizontally
wpose.position.y += scale * 0.1
waypoints.append(copy.deepcopy(wpose))

# 3.2 move to central point
wpose.position.y -= scale * 0.05
waypoints.append(copy.deepcopy(wpose))

# 3.3 move vertically down
wpose.position.z -= scale * 0.1
waypoints.append(copy.deepcopy(wpose))

# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space, which is sufficient
# for this tutorial.
(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

#Call the planner to compute the plan and execute
plan = move_group.go(wait = True)

# if want to print joint angles, use this instead of move_group.go:
#move_group.execute(plan, wait=True)

# find and print current joint poses
# joint1 = move_group.get_current_joint_values()
# print("joint1: ",joint1)

#Stop to ensure no residual movement
move_group.stop()
#clear targets after planning with poses
move_group.clear_pose_targets()

