#!/usr/bin/env python3

#Pyhon 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander #import the moveit_commander namespace
#provides MoveGroupCommander class, PlanningSceneInterface class, and RobotCommander class
import moveit_msgs.msg #import messages used
import geometry_msgs.msg #import messages

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

#Initialize moveit_commander and a rospy node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

#Instantiate a RobotCommander object and a PlanningSceneInterface object
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

#Instantiate MoveGroupCommander, an interface to a planning group (a group of joints)
move_group = moveit_commander.MoveGroupCommander("manipulator")
#Display trajectories in Rviz
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

scale = 1.0
waypoints = []

wpose = move_group.get_current_pose().pose
wpose.position.x += scale * 0.2  # Move sideways (x)
waypoints.append(copy.deepcopy(wpose))

wpose.position.x -= scale * 0.2  # Second move down and sideways (x)
wpose.position.z -= scale * 0.2 
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.2  # Third move sideways (x)
waypoints.append(copy.deepcopy(wpose))

# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space, which is sufficient
# for this tutorial.
(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

print(plan)

#Call the planner to compute the plan and execute
plan = move_group.go(wait = True)
#Stop to ensure no residual movement
move_group.stop()
#clear targets after planning with poses
move_group.clear_pose_targets()


#move_group.execute(plan, wait=True)
